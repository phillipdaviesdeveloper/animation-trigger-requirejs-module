define(function()
    {

        var debugMode = true;

        var log = function (msg) {
            debugMode && window.console && console.log ? console.log(msg) : null;
        };

        return{
            log: log
        }

    }
);