# Hi!

The main js organises and calls the required modules (jquery, log, anim-position), a config object is then passed to the new anim-position instance
to set up all the bits and bobs.

This module provides a standard object for configuring animation css rules to be applied by javascript when a specified trigger is fired in the browser.

Each new trigger requires a new instance of the anim-position module and a new init method to be called with a config object.

Configuration allows the following settings:

* target String -  Identifys the targeted DOM items.
* anim String - The animation class to be applied to the target (must be a class).
* pos String or INT - Either sets a defined trigger value or is a DOM element to calculate the trigger value from.
* modifer INT - Allows a pixel amount to be added to tweak the animation trigger by adding or subtracting from the trigger.
* type String - Either addClass or RemoveClass, default is removeClass. Tells the module to either remove or add the defined anim class.


