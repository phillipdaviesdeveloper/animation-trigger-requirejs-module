require.config({
    baseUrl: "*add a base url for your modules here*",
    shim: {
        jquery: {
            exports: 'jQuery'
        }
    },

    paths: {
        jquery: [
            "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min",
            "vendor/jquery"
        ]
    }
});

requirejs(['jquery', 'modules/anim-position', 'modules/log'], function ($, animpos, log) {


    try
    {

        //check if a dom item is present
        if($('.logo--menu').length)
        {
            //if it is lets create an istance of the anim-postion module
            var logoAnim = new animpos;

            //lets configure the instance to our preferences (see modules/anim-position for configuration option details)
            logoAnim.init({
                target: '.logo--menu',
                anim: 'is--sliding',
                pos: '.splash--homepage',
                modifer: -80,
                type: 'removeClass',
                on:  'scroll'
            });

            //ok lets set up an individual timer to check the user's browser position
            var logoAnimTimeout = setInterval(function() {

                //call the watchTrigger method, if it returns true then stop checking for the trigger and clear the timeout
                if(logoAnim.watchTrigger(logoAnim))
                {
                    clearInterval(logoAnimTimeout);
                }

            }, 100);
        }

    }
    catch(e)
    {
        log.log(e);
    }


});